@N: MF248 |Running in 64-bit mode.
@N: MF667 |Clock conversion disabled. (Command "set_option -fix_gated_and_generated_clocks 0" in the project file.)
@N: FX1056 |Writing EDF file: E:\Projects\Libero\ALS\synthesis\MicroSS.edn
@N: BW103 |The default time unit for the Synopsys Constraint File (SDC or FDC) is 1ns.
@N: BW107 |Synopsys Constraint File capacitance units using default value of 1pF 
@N: MT320 |This timing report is an estimate of place and route data. For final timing results, use the FPGA vendor place and route report.
@N: MT322 |Clock constraints include only register-to-register paths associated with each individual clock.
@N: MT582 |Estimated period and frequency not reported for given clock unless the clock has at least one timing path which is not a false or a max delay path and that does not have excessive slack
