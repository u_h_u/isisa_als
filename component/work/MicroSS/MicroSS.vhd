----------------------------------------------------------------------
-- Created by SmartDesign Wed Jan 23 17:05:40 2019
-- Version: v11.9 SP2 11.9.2.1
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- MicroSS entity declaration
----------------------------------------------------------------------
entity MicroSS is
    -- Port list
    port(
        -- Inputs
        DEVRST_N         : in    std_logic;
        GPIO_8_F2M       : in    std_logic;
        GPIO_9_F2M       : in    std_logic;
        MMUART_0_RXD_F2M : in    std_logic;
        -- Outputs
        GPIO_0_M2F       : out   std_logic;
        GPIO_10_M2F      : out   std_logic;
        GPIO_1_M2F       : out   std_logic;
        GPIO_2_M2F       : out   std_logic;
        GPIO_3_M2F       : out   std_logic;
        GPIO_4_M2F       : out   std_logic;
        GPIO_5_M2F       : out   std_logic;
        GPIO_6_M2F       : out   std_logic;
        GPIO_7_M2F       : out   std_logic;
        MMUART_0_TXD_M2F : out   std_logic;
        SPI_1_CLK_M2F    : out   std_logic;
        SPI_1_DO_M2F     : out   std_logic;
        -- Inouts
        SCL              : inout std_logic;
        SDA              : inout std_logic;
        SS               : inout std_logic
        );
end MicroSS;
----------------------------------------------------------------------
-- MicroSS architecture body
----------------------------------------------------------------------
architecture RTL of MicroSS is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- BIBUF
component BIBUF
    generic( 
        IOSTD : string := "" 
        );
    -- Port list
    port(
        -- Inputs
        D   : in    std_logic;
        E   : in    std_logic;
        -- Outputs
        Y   : out   std_logic;
        -- Inouts
        PAD : inout std_logic
        );
end component;
-- MicroSS_sb
component MicroSS_sb
    -- Port list
    port(
        -- Inputs
        DEVRST_N         : in  std_logic;
        FAB_RESET_N      : in  std_logic;
        GPIO_8_F2M       : in  std_logic;
        GPIO_9_F2M       : in  std_logic;
        I2C_0_SCL_F2M    : in  std_logic;
        I2C_0_SDA_F2M    : in  std_logic;
        MMUART_0_RXD_F2M : in  std_logic;
        SPI_1_CLK_F2M    : in  std_logic;
        SPI_1_DI_F2M     : in  std_logic;
        SPI_1_SS0_F2M    : in  std_logic;
        -- Outputs
        FAB_CCC_GL0      : out std_logic;
        FAB_CCC_LOCK     : out std_logic;
        GPIO_0_M2F       : out std_logic;
        GPIO_10_M2F      : out std_logic;
        GPIO_1_M2F       : out std_logic;
        GPIO_2_M2F       : out std_logic;
        GPIO_3_M2F       : out std_logic;
        GPIO_4_M2F       : out std_logic;
        GPIO_5_M2F       : out std_logic;
        GPIO_6_M2F       : out std_logic;
        GPIO_7_M2F       : out std_logic;
        I2C_0_SCL_M2F    : out std_logic;
        I2C_0_SCL_M2F_OE : out std_logic;
        I2C_0_SDA_M2F    : out std_logic;
        I2C_0_SDA_M2F_OE : out std_logic;
        INIT_DONE        : out std_logic;
        MMUART_0_TXD_M2F : out std_logic;
        MSS_READY        : out std_logic;
        POWER_ON_RESET_N : out std_logic;
        SPI_1_CLK_M2F    : out std_logic;
        SPI_1_DO_M2F     : out std_logic;
        SPI_1_SS0_M2F    : out std_logic;
        SPI_1_SS0_M2F_OE : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal BIBUF_0_Y                     : std_logic;
signal BIBUF_1_Y                     : std_logic;
signal BIBUF_2_Y                     : std_logic;
signal GPIO_0_M2F_net_0              : std_logic;
signal GPIO_1_M2F_net_0              : std_logic;
signal GPIO_2_M2F_net_0              : std_logic;
signal GPIO_3_M2F_net_0              : std_logic;
signal GPIO_4_M2F_net_0              : std_logic;
signal GPIO_5_M2F_net_0              : std_logic;
signal GPIO_6_M2F_net_0              : std_logic;
signal GPIO_7_M2F_net_0              : std_logic;
signal GPIO_10_M2F_net_0             : std_logic;
signal MicroSS_sb_0_I2C_0_SCL_M2F    : std_logic;
signal MicroSS_sb_0_I2C_0_SCL_M2F_OE : std_logic;
signal MicroSS_sb_0_I2C_0_SDA_M2F    : std_logic;
signal MicroSS_sb_0_I2C_0_SDA_M2F_OE : std_logic;
signal MicroSS_sb_0_POWER_ON_RESET_N : std_logic;
signal MicroSS_sb_0_SPI_1_SS0_M2F    : std_logic;
signal MicroSS_sb_0_SPI_1_SS0_M2F_OE : std_logic;
signal MMUART_0_TXD_M2F_net_0        : std_logic;
signal SPI_1_CLK_M2F_net_0           : std_logic;
signal SPI_1_DO_M2F_net_0            : std_logic;
signal MMUART_0_TXD_M2F_net_1        : std_logic;
signal GPIO_0_M2F_net_1              : std_logic;
signal GPIO_1_M2F_net_1              : std_logic;
signal GPIO_2_M2F_net_1              : std_logic;
signal GPIO_3_M2F_net_1              : std_logic;
signal GPIO_4_M2F_net_1              : std_logic;
signal GPIO_5_M2F_net_1              : std_logic;
signal GPIO_6_M2F_net_1              : std_logic;
signal GPIO_7_M2F_net_1              : std_logic;
signal GPIO_10_M2F_net_1             : std_logic;
signal SPI_1_DO_M2F_net_1            : std_logic;
signal SPI_1_CLK_M2F_net_1           : std_logic;
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net                       : std_logic;

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net <= '0';
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 MMUART_0_TXD_M2F_net_1 <= MMUART_0_TXD_M2F_net_0;
 MMUART_0_TXD_M2F       <= MMUART_0_TXD_M2F_net_1;
 GPIO_0_M2F_net_1       <= GPIO_0_M2F_net_0;
 GPIO_0_M2F             <= GPIO_0_M2F_net_1;
 GPIO_1_M2F_net_1       <= GPIO_1_M2F_net_0;
 GPIO_1_M2F             <= GPIO_1_M2F_net_1;
 GPIO_2_M2F_net_1       <= GPIO_2_M2F_net_0;
 GPIO_2_M2F             <= GPIO_2_M2F_net_1;
 GPIO_3_M2F_net_1       <= GPIO_3_M2F_net_0;
 GPIO_3_M2F             <= GPIO_3_M2F_net_1;
 GPIO_4_M2F_net_1       <= GPIO_4_M2F_net_0;
 GPIO_4_M2F             <= GPIO_4_M2F_net_1;
 GPIO_5_M2F_net_1       <= GPIO_5_M2F_net_0;
 GPIO_5_M2F             <= GPIO_5_M2F_net_1;
 GPIO_6_M2F_net_1       <= GPIO_6_M2F_net_0;
 GPIO_6_M2F             <= GPIO_6_M2F_net_1;
 GPIO_7_M2F_net_1       <= GPIO_7_M2F_net_0;
 GPIO_7_M2F             <= GPIO_7_M2F_net_1;
 GPIO_10_M2F_net_1      <= GPIO_10_M2F_net_0;
 GPIO_10_M2F            <= GPIO_10_M2F_net_1;
 SPI_1_DO_M2F_net_1     <= SPI_1_DO_M2F_net_0;
 SPI_1_DO_M2F           <= SPI_1_DO_M2F_net_1;
 SPI_1_CLK_M2F_net_1    <= SPI_1_CLK_M2F_net_0;
 SPI_1_CLK_M2F          <= SPI_1_CLK_M2F_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- BIBUF_0
BIBUF_0 : BIBUF
    port map( 
        -- Inputs
        D   => MicroSS_sb_0_SPI_1_SS0_M2F,
        E   => MicroSS_sb_0_SPI_1_SS0_M2F_OE,
        -- Outputs
        Y   => BIBUF_0_Y,
        -- Inouts
        PAD => SS 
        );
-- BIBUF_1
BIBUF_1 : BIBUF
    port map( 
        -- Inputs
        D   => MicroSS_sb_0_I2C_0_SDA_M2F,
        E   => MicroSS_sb_0_I2C_0_SDA_M2F_OE,
        -- Outputs
        Y   => BIBUF_1_Y,
        -- Inouts
        PAD => SDA 
        );
-- BIBUF_2
BIBUF_2 : BIBUF
    port map( 
        -- Inputs
        D   => MicroSS_sb_0_I2C_0_SCL_M2F,
        E   => MicroSS_sb_0_I2C_0_SCL_M2F_OE,
        -- Outputs
        Y   => BIBUF_2_Y,
        -- Inouts
        PAD => SCL 
        );
-- MicroSS_sb_0
MicroSS_sb_0 : MicroSS_sb
    port map( 
        -- Inputs
        FAB_RESET_N      => MicroSS_sb_0_POWER_ON_RESET_N,
        DEVRST_N         => DEVRST_N,
        SPI_1_DI_F2M     => GND_net,
        SPI_1_CLK_F2M    => GND_net,
        SPI_1_SS0_F2M    => BIBUF_0_Y,
        MMUART_0_RXD_F2M => MMUART_0_RXD_F2M,
        I2C_0_SDA_F2M    => BIBUF_1_Y,
        I2C_0_SCL_F2M    => BIBUF_2_Y,
        GPIO_8_F2M       => GPIO_8_F2M,
        GPIO_9_F2M       => GPIO_9_F2M,
        -- Outputs
        POWER_ON_RESET_N => MicroSS_sb_0_POWER_ON_RESET_N,
        INIT_DONE        => OPEN,
        FAB_CCC_GL0      => OPEN,
        FAB_CCC_LOCK     => OPEN,
        MSS_READY        => OPEN,
        SPI_1_DO_M2F     => SPI_1_DO_M2F_net_0,
        SPI_1_CLK_M2F    => SPI_1_CLK_M2F_net_0,
        SPI_1_SS0_M2F    => MicroSS_sb_0_SPI_1_SS0_M2F,
        SPI_1_SS0_M2F_OE => MicroSS_sb_0_SPI_1_SS0_M2F_OE,
        MMUART_0_TXD_M2F => MMUART_0_TXD_M2F_net_0,
        I2C_0_SDA_M2F    => MicroSS_sb_0_I2C_0_SDA_M2F,
        I2C_0_SDA_M2F_OE => MicroSS_sb_0_I2C_0_SDA_M2F_OE,
        I2C_0_SCL_M2F    => MicroSS_sb_0_I2C_0_SCL_M2F,
        I2C_0_SCL_M2F_OE => MicroSS_sb_0_I2C_0_SCL_M2F_OE,
        GPIO_0_M2F       => GPIO_0_M2F_net_0,
        GPIO_1_M2F       => GPIO_1_M2F_net_0,
        GPIO_2_M2F       => GPIO_2_M2F_net_0,
        GPIO_3_M2F       => GPIO_3_M2F_net_0,
        GPIO_4_M2F       => GPIO_4_M2F_net_0,
        GPIO_5_M2F       => GPIO_5_M2F_net_0,
        GPIO_6_M2F       => GPIO_6_M2F_net_0,
        GPIO_7_M2F       => GPIO_7_M2F_net_0,
        GPIO_10_M2F      => GPIO_10_M2F_net_0 
        );

end RTL;
