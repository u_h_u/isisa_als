set_component MicroSS_sb_FABOSC_0_OSC
# Microsemi Corp.
# Date: 2019-Jan-23 17:05:35
#

create_clock -ignore_errors -period 20 [ get_pins { I_RCOSC_25_50MHZ/CLKOUT } ]
