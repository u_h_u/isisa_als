probe \
    -desdir {E:\Projects\Libero\ALS\designer\MicroSS} \
    -design MicroSS \
    -fam SmartFusion2 \
    -die PA4M1000_N \
    -pkg tq144 \
    -use_mvn_pdc 0  \
    -use_last_placement 0 