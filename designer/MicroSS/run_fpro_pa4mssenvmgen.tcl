set_device \
    -fam SmartFusion2 \
    -die PA4M1000_N \
    -pkg tq144
set_input_cfg \
	-path {E:/Projects/Libero/ALS/component/work/MicroSS_sb_MSS/ENVM.cfg}
set_output_efc \
    -path {E:\Projects\Libero\ALS\designer\MicroSS\MicroSS.efc}
set_proj_dir \
    -path {E:\Projects\Libero\ALS}
gen_prg -use_init false
