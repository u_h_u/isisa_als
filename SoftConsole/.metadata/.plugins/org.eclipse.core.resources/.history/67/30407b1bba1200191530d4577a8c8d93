/*
 * main.c
 *
 *  Created on: Jan 7, 2019
 *      Author: tm5683
 */


#include "drivers/mss_i2c/mss_i2c.h" //I2C functions
#include "drivers/mss_uart/mss_uart.h" //UART functions
#include "CMSIS/system_m2sxxx.h" //Startup configuration + SystemCoreClock etc.
/*==============================================================================
Private functions.
*/
void delay(uint32_t amount_ms);
void ALS_init();
/*==============================================================================
* main() function.
*/
const uint8_t g_greeting_msg[] =
"\r\n\r\n\
**********************************************************************\r\n\
************ Ambient Light Sensor Characterization Project ***********\r\n\
**********************************************************************\r\n\
UART FUNCTIONING.													  \r\n\
---------------------------------------------------------------------\r\n";
const uint8_t g_i2c_init_msg[] =
"I2C INITIALIZED.													  \r\n\
---------------------------------------------------------------------\r\n";

/*==============================================================================
* Register and mask defines for the Ambient Light Sensor
*/
#define ALS_ADR 0x29	// i2c address of the ALS - for read append 1, write 0
#define ALS_CONTR_REG 0x80	// reg for controling ALS operations
#define ALS_MODE 0x1		// mask for ALS mode in CONTR_REG
#define ALS_ACTIVE_MODE 0x1	// value for ALS_MODE to enable measurements
#define ALS_GAIN 0x1C		// mask for ALS gain in CONTR_REG
#define ALS_GAIN_1X 0x0		// set ALS_GAIN to 1x
#define ALS_GAIN_2X 0x1		// set ALS_GAIN to 2x
#define ALS_GAIN_4X 0x2		// set ALS_GAIN to 4x
#define ALS_GAIN_8X 0x3		// set ALS_GAIN to 8x
#define ALS_GAIN_48X 0x6	// set ALS_GAIN to 48x
#define ALS_GAIN_96X 0x7	// set ALS_GAIN to 96x
#define ALS_MEAS_RATE_REG 0x85	// controls integration time and timing of periodic measurement
#define ALS_INT_TIME 0x38		// mask for ALS_MEAS_RATE to set integration time
#define ALS_MEAS_RATE 0x07		// mask for ALS_MEAS_RATE to set period of measurements
// ALS_MEAS_RATE must be equal or greater than ALS_INT_TIME
#define ALS_MEAS_RATE_50 0x0	// set ALS_MEAS_RATE to 50 ms
#define ALS_MEAS_RATE_100 0x1	// set ALS_MEAS_RATE to 100 ms
#define ALS_MEAS_RATE_200 0x2	// set ALS_MEAS_RATE to 200 ms
#define ALS_MEAS_RATE_500 0x3	// set ALS_MEAS_RATE to 500 ms
#define ALS_MEAS_RATE_1000 0x4	// set ALS_MEAS_RATE to 1000 ms
#define ALS_MEAS_RATE_2000 0x5	// set ALS_MEAS_RATE to 2000 ms
#define ALS_INT_TIME_100 0x0	// set ALS_INT_TIME to 100 ms
#define ALS_INT_TIME_50 0x1		// set ALS_INT_TIME to 50 ms
#define ALS_INT_TIME_200 0x2	// set ALS_INT_TIME to 200 ms
#define ALS_INT_TIME_400 0x3	// set ALS_INT_TIME to 400 ms
#define ALS_INT_TIME_150 0x4	// set ALS_INT_TIME to 150 ms
#define ALS_INT_TIME_250 0x5	// set ALS_INT_TIME to 250 ms
#define ALS_INT_TIME_300 0x6	// set ALS_INT_TIME to 300 ms
#define ALS_INT_TIME_350 0x7	// set ALS_INT_TIME to 350 ms
#define ALS_DATA_CH1_REG_LO	0x88	// lower part of reg holding CH1 meas
#define ALS_DATA_CH1_REG_HI	0x89	// higher part of reg holding CH1 meas
#define ALS_DATA_CH0_REG_LO	0x8A	// lower part of reg holding CH0 meas
#define ALS_DATA_CH0_REG_HI	0x8B	// higher part of reg holding CH0 meas
#define ALS_STATUS_REG 0x8C		// reg containing status of data - new 1, old 0
#define ALS_STATUS_DATA 0x4		// mask for getting status of data
#define ALS_STATUS_NEW_DATA 0x1	// value denoting new data from meas




int main(){

	// Initialize UART interface for communication with PC
	MSS_UART_init(&g_mss_uart0,
				  MSS_UART_57600_BAUD,
				  MSS_UART_DATA_8_BITS |
				  MSS_UART_NO_PARITY |
				  MSS_UART_ONE_STOP_BIT);

	// Write Greetings Message
	MSS_UART_polled_tx_string(&g_mss_uart0, g_greeting_msg);

	// Initialize I2C interface for communication with ALS
	MSS_I2C_init(&g_mss_i2c0,
				  0x0,	// address if in slave configuration - not needed here
				  MSS_I2C_PCLK_DIV_960);
	// Write I2C Initialization Message
	MSS_UART_polled_tx_string(&g_mss_uart0, g_i2c_init_msg);

	// Initialize ALS
	ALS_init();


	int i = 1+2;

	return 0;
}
/*==============================================================================
Delay
- function produces a delay for amount_ms (in ms)
*/
void delay(uint32_t amount_ms){
	// SystemCoreClock = 100000000u  ...  1/100MHz
	// count = x  ...  amount_ms
	uint32_t count = SystemCoreClock*amount_ms / 32u;
	while(count > 0)	{
	--count;
	}
}
/*==============================================================================
ALS_init
*/
void ALS_init(){
	// wait for 100 ms to ensure it is correctly power up
	delay(100);
}
}
