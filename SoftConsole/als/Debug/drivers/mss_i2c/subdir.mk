################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/mss_i2c/mss_i2c.c 

OBJS += \
./drivers/mss_i2c/mss_i2c.o 

C_DEPS += \
./drivers/mss_i2c/mss_i2c.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/mss_i2c/%.o: ../drivers/mss_i2c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -std=gnu11 --specs=cmsis.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


