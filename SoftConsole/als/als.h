/*
 * als.h
 *
 *  Created on: Jan 12, 2019
 *      Author: Uros Hudomalj
 */

#ifndef ALS_H_
#define ALS_H_

/*############################### INCLUDES ###################################*/
#include "drivers/mss_i2c/mss_i2c.h" //I2C functions
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "CMSIS/system_m2sxxx.h" //Startup configuration + SystemCoreClock etc.

/*############################### DEFINES ####################################*/
#define DATA_LENGTH_WRITE 2	// addr of regs of ALS are 1 Byte long + 1 Byte for value you want to write in
#define DATA_LENGTH_READ 1	// 1 Byte for value of the reg
/*==============================================================================
* Register and mask defines for the Ambient Light Sensor
*/
#define ALS_ADDR 0x29	// i2c address of the ALS - for read append 1, write 0
#define ALS_CONTR_REG 0x80	// reg for controling ALS operations
#define ALS_MODE 0x1		// mask for ALS mode in CONTR_REG
#define ALS_ACTIVE_MODE 0x1	// value for ALS_MODE to enable measurements
#define ALS_GAIN 0x1C		// mask for ALS gain in CONTR_REG
#define ALS_GAIN_1X 0x0		// set ALS_GAIN to 1x
#define ALS_GAIN_2X 0x1		// set ALS_GAIN to 2x
#define ALS_GAIN_4X 0x2		// set ALS_GAIN to 4x
#define ALS_GAIN_8X 0x3		// set ALS_GAIN to 8x
#define ALS_GAIN_48X 0x6	// set ALS_GAIN to 48x
#define ALS_GAIN_96X 0x7	// set ALS_GAIN to 96x
#define ALS_MEAS_RATE_REG 0x85	// controls integration time and timing of periodic measurement
#define ALS_INT_TIME 0x38		// mask for ALS_MEAS_RATE to set integration time
#define ALS_MEAS_RATE 0x07		// mask for ALS_MEAS_RATE to set period of measurements
// ALS_MEAS_RATE must be equal or greater than ALS_INT_TIME
#define ALS_MEAS_RATE_50 0x0	// set ALS_MEAS_RATE to 50 ms
#define ALS_MEAS_RATE_100 0x1	// set ALS_MEAS_RATE to 100 ms
#define ALS_MEAS_RATE_200 0x2	// set ALS_MEAS_RATE to 200 ms
#define ALS_MEAS_RATE_500 0x3	// set ALS_MEAS_RATE to 500 ms
#define ALS_MEAS_RATE_1000 0x4	// set ALS_MEAS_RATE to 1000 ms
#define ALS_MEAS_RATE_2000 0x5	// set ALS_MEAS_RATE to 2000 ms
#define ALS_INT_TIME_100 0x0	// set ALS_INT_TIME to 100 ms
#define ALS_INT_TIME_50 0x1		// set ALS_INT_TIME to 50 ms
#define ALS_INT_TIME_200 0x2	// set ALS_INT_TIME to 200 ms
#define ALS_INT_TIME_400 0x3	// set ALS_INT_TIME to 400 ms
#define ALS_INT_TIME_150 0x4	// set ALS_INT_TIME to 150 ms
#define ALS_INT_TIME_250 0x5	// set ALS_INT_TIME to 250 ms
#define ALS_INT_TIME_300 0x6	// set ALS_INT_TIME to 300 ms
#define ALS_INT_TIME_350 0x7	// set ALS_INT_TIME to 350 ms
#define ALS_DATA_CH1_REG_LO	0x88	// lower part of reg holding CH1 meas
#define ALS_DATA_CH1_REG_HI	0x89	// higher part of reg holding CH1 meas
#define ALS_DATA_CH0_REG_LO	0x8A	// lower part of reg holding CH0 meas
#define ALS_DATA_CH0_REG_HI	0x8B	// higher part of reg holding CH0 meas
#define ALS_STATUS_REG 0x8C		// reg containing status of data - new 1, old 0
#define ALS_STATUS_DATA 0x4		// mask for getting status of data
#define ALS_STATUS_NEW_DATA 0x1	// value denoting new data from meas
#define ALS_PART_ID_REG 0x86	// register containing part id number

/*###################### FUNCTIONS' DECLERATIONS #############################*/
/*==============================================================================
Delay
- function produces a delay for amount_ms (in ms)
*/
void delay(uint32_t amount_ms);
/*==============================================================================
ALS_init
- initializes the ALS
- also set the gain, measure rate and integration time
- returns 0 on success, else code of error
*/
uint8_t ALS_init();
/*==============================================================================
set_reg_to
- function sets masked bits of reg to value
- return 1 on failure, 0 on success
*/
uint8_t set_reg_to(uint8_t reg, uint8_t mask, uint8_t val);
/*==============================================================================
read_als
- function reads ALS measurements if new data is available
- in case new data is available, it stores them by byte into data[4]
- return 1 on successfully reading new data, else 0
*/
uint8_t read_als(uint8_t *data);
/*==============================================================================
format_als_raw_data
- function parses the input als_raw_data of size 4 into string containing
  measurements for Ch0 and Ch1 in lux (according to the gain)
*/
uint8_t * format_als_raw_data(uint8_t *als_raw_data);

#endif /* ALS_H_ */
