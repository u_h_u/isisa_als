/*
 * als.c
 *
 *  Created on: Jan 12, 2019
 *      Author: Uros Hudomalj
 */

#include "als.h"

/*==============================================================================
Delay
- function produces a delay for amount_ms (in ms)
*/
void delay(uint32_t amount_ms){
	// SystemCoreClock = 100000000u = 100M(Hz)
	// count = x ... 1/amount_ms
	// f = N/t => N = f * t
	uint32_t count = SystemCoreClock * amount_ms / 1000;
	while(count > 0){
	--count;
	}
}
/*==============================================================================
ALS_init
- initializes the ALS
- also set the gain, measure rate and integration time
- returns 0 on success, else code of error
*/
uint8_t ALS_init(){
	uint8_t ok = 0;

	// wait for 100 ms to ensure it is correctly powered up
	delay(100);
	// set MODE of ALS to active
	ok = set_reg_to(ALS_CONTR_REG, ALS_MODE, ALS_ACTIVE_MODE);
	if(ok==1) return 1;
	// wait for 10 ms to ensure valid mode switch
	delay(10);
	// set gain
	ok = set_reg_to(ALS_CONTR_REG, ALS_GAIN, ALS_GAIN_8X);
	if(ok==1) return 2;
	// set measure rate
	ok = set_reg_to(ALS_MEAS_RATE_REG, ALS_MEAS_RATE, ALS_MEAS_RATE_100);
	if(ok==1) return 3;
	// set integration time
	ok = set_reg_to(ALS_MEAS_RATE_REG, ALS_INT_TIME, ALS_INT_TIME_100);
	if(ok==1) return 4;

	return 0;
}
/*==============================================================================
set_reg_to
- function sets masked bits of reg to value
- return 1 on failure, 0 on success
*/
uint8_t set_reg_to(uint8_t reg, uint8_t mask, uint8_t val){
	uint8_t  rx_buffer[DATA_LENGTH_READ];
	uint8_t  read_length = DATA_LENGTH_READ;
	uint8_t  tx_buffer[DATA_LENGTH_WRITE];
	uint8_t  write_length = DATA_LENGTH_WRITE;

	uint8_t addr = ALS_ADDR;

	mss_i2c_status_t status;

	tx_buffer[0] = reg;	// denotes the reg from which to read
	tx_buffer[1] = 0;
	// Read data already written in the reg
	addr = ALS_ADDR;
	MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
						read_length, rx_buffer, read_length,
						MSS_I2C_RELEASE_BUS );
	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
	if(status != MSS_I2C_SUCCESS){
		return 1;	// error while using i2c
	}

	tx_buffer[0] = reg;	// denotes the reg from which to read
	// Clear the bits you want to set
	tx_buffer[1] = (rx_buffer[0] & (~mask));
	// Get how many times does the value have to be shifted to get into position
	uint8_t shift = 0;
	while((mask & (1<<shift)) == 0 ){
		shift++;
	}
	// Set the bits accordingly
	tx_buffer[1] |= (val << shift);	 // set the masked bit
	// Write the new data into reg
	addr = ALS_ADDR;
	MSS_I2C_write( &g_mss_i2c0, addr, tx_buffer, write_length,
				  MSS_I2C_RELEASE_BUS );
	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
	if(status != MSS_I2C_SUCCESS){
		return 1;	// error while using i2c
	}

	// FOR TESTING: to check if the value was correctly written
/*	tx_buffer[0]=ALS_CONTR_REG;
	rx_buffer[0]=250;
	MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
							read_length, rx_buffer, read_length,
							MSS_I2C_RELEASE_BUS );
	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
*/
	return 0;
}
/*==============================================================================
read_als
- function reads ALS measurements if new data is available
- in case new data is available, it stores them by byte into data[4]
- return 1 on successfully reading new data, else 0
*/
uint8_t read_als(uint8_t *data){
	uint8_t  rx_buffer[DATA_LENGTH_READ];
	uint8_t  read_length = DATA_LENGTH_READ;
	uint8_t  tx_buffer[DATA_LENGTH_READ];		// used just for reading from reg
	uint8_t  write_length = DATA_LENGTH_READ;

	uint8_t addr = ALS_ADDR;

	mss_i2c_status_t status;

	// Read the ALS_STATUS_REG to see if new data is available
	tx_buffer[0] = ALS_STATUS_REG;	// denotes the reg from which to read
	// Read data in the reg
	MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
						write_length, rx_buffer, read_length,
						MSS_I2C_RELEASE_BUS );
	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
	if(status != MSS_I2C_SUCCESS){
		return 0;	// error while using i2c
	}

	// If there is no new data, return 0
	if( (rx_buffer[0] & ALS_STATUS_DATA) == 0) return 0;

	// Else read all the registers containing measurements of both channels
	tx_buffer[0] = ALS_DATA_CH1_REG_LO;	// denotes 1st reg from which to read
	for(uint8_t offset=0; offset<4; offset++){
		// Read data in the reg
		MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
							write_length, rx_buffer, read_length,
							MSS_I2C_RELEASE_BUS );
		status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
		if(status != MSS_I2C_SUCCESS){
			return 0;	// error while using i2c
		}

		// Save the read data to data buffer
		data[offset] = rx_buffer[0];

		// Move to next register
		switch(offset){
			case 0: tx_buffer[0] = ALS_DATA_CH1_REG_HI; break;
			case 1: tx_buffer[0] = ALS_DATA_CH0_REG_LO; break;
			case 2: tx_buffer[0] = ALS_DATA_CH0_REG_HI; break;
			default: tx_buffer[0] = ALS_PART_ID_REG; break;
		}
	}

/*	//TEST
 	rx_buffer[0]=250;
	rx_buffer[1]=250;

	tx_buffer[0] = ALS_PART_ID_REG;

    MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
                        write_length, rx_buffer, 1,
                        MSS_I2C_RELEASE_BUS );

    status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
    if(status != MSS_I2C_SUCCESS){
		return 0;	// error while using i2c
	}

    tx_buffer[0] = ALS_PART_ID_REG+1;

	MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
						write_length, rx_buffer, 1,
						MSS_I2C_RELEASE_BUS );

	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
	if(status != MSS_I2C_SUCCESS){
		return 0;	// error while using i2c
	}
*/

	return 1;
}

/*==============================================================================
format_als_raw_data
- function parses the input als_raw_data of size 4 into string containing
  measurements for Ch0 and Ch1 in lux (according to the gain)
*/
uint8_t * format_als_raw_data(uint8_t *als_raw_data){
	uint8_t  rx_buffer[1];
	uint8_t  read_length = 1;
	uint8_t  tx_buffer[1];	// used just for reading from reg
	uint8_t  write_length = 1;

	uint8_t addr = ALS_ADDR;

	mss_i2c_status_t status;

	// Get the gain of ALS
	tx_buffer[0] = ALS_CONTR_REG;	// denotes the reg from which to read
	// Read data in the reg
	MSS_I2C_write_read( &g_mss_i2c0, addr, tx_buffer,
						write_length, rx_buffer, read_length,
						MSS_I2C_RELEASE_BUS );
	status = MSS_I2C_wait_complete( &g_mss_i2c0, MSS_I2C_NO_TIMEOUT );
	if(status != MSS_I2C_SUCCESS){
		return NULL;	// error while using i2c
	}

	uint8_t gain_bin = (rx_buffer[0] & ALS_GAIN) >> 2;
	uint8_t gain = 0;
	switch(gain_bin){
		case ALS_GAIN_1X: gain=1; break;
		case ALS_GAIN_2X: gain=2; break;
		case ALS_GAIN_4X: gain=4; break;
		case ALS_GAIN_8X: gain=8; break;
		case ALS_GAIN_48X: gain=48; break;
		case ALS_GAIN_96X: gain=96; break;
		default: gain=1; break;
	}

	// Join LO and HI bits to form 16 bit value
	uint16_t ch0 = (als_raw_data[3]<<8) + als_raw_data[2];
	uint16_t ch1 = (als_raw_data[1]<<8) + als_raw_data[0];

	// Convert to floats according to gain
	float ch0_f = ch0/(float)gain;
	float ch1_f = ch1/(float)gain;

	// Formated string
	uint8_t *formated = malloc(40);

	// Populate the string with ch float values (format %6.2f)
	int c = 0;
	int j=0;
	int tmp=0;
	float dec = 10000;
	// ch0
	for(j=0; j<8; j++){
		if(j==5){
			formated[c] = '.'; // insert '.'
		}
		else{
			tmp = (int)(ch0_f/dec);
			tmp = tmp%10;
			tmp += '0';
			formated[c] = tmp;
			dec/=10;
		}
		c++;
	}
	formated[c]=',';
	c++;
	formated[c]=' ';
	c++;
	dec = 10000;
	// ch1
	for(j=0; j<8; j++){
		if(j==5){
			formated[c] = '.'; // insert '.'
		}
		else{
			tmp = (int)(ch1_f/dec);
			tmp = tmp%10;
			tmp += '0';
			formated[c] = tmp;
			dec/=10;
		}
		c++;
	}
	formated[c]='\r';
	c++;
	formated[c]='\n';
	c++;
	formated[c]=0;
	c++;

	return formated;
}
