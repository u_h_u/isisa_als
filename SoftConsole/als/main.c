/*
 * main.c
 *
 *  Created on: Jan 7, 2019
 *  Last revision on: Jan 24, 2019
 *      Author: Gasper Skvarc Bozic & Uros Hudomalj
 */


#include "als.h"	//ALS functions
#include "drivers/mss_uart/mss_uart.h" //UART functions
#include "CMSIS/system_m2sxxx.h" //Startup configuration + SystemCoreClock etc.
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "drivers\mss_gpio\mss_gpio.h"
#include "drivers\mss_timer\mss_timer.h"
#include "drivers\mss_spi\mss_spi.h"
#include "MicroSS_hw_platform.h"
#include "AD9833.h"

#define TIME_SLOT				 				  5  // time slot for each pwm frequency, min value allowed is 1, suggested value is 5
#define NUM_OF_DECADES		 	 				  3  // => frequency range is 1Hz - 10^NUM_OF_DECADES
#define POINTS_PER_DECADE						 50  //
//#define FREQ_STEP			(10 / POINTS_PER_DECADE) //

#define GPIO_LED MSS_GPIO_7	// gpio number to which slight source is connected

/*==============================================================================
Private functions.
*/

/*==============================================================================
* main() function.
*/
const uint8_t g_greeting_msg[] =
"\r\n\r\n\
**********************************************************************\r\n\
************ Ambient Light Sensor Characterization Project ***********\r\n\
**********************************************************************\r\n\
UART FUNCTIONING.													  \r\n";
const uint8_t g_i2c_init_msg[] =
"I2C INITIALIZED.													  \r\n";
const uint8_t g_als_init_msg[] =
"ALS INITIALIZED.													  \r\n";
const uint8_t g_new_freq_mess[] =
"New frequency is set												  \r\n";

uint8_t change_freq = 1; // this flag is activated by Timer1, it tells us when to change pwm frequency
const double FREQ_STEP = (10-1) / (double) (POINTS_PER_DECADE-1);

int main(){
	// Initialize UART interface for communication with PC
	MSS_UART_init(&g_mss_uart0,
				  MSS_UART_57600_BAUD,
				  MSS_UART_DATA_8_BITS |
				  MSS_UART_NO_PARITY |
				  MSS_UART_ONE_STOP_BIT);

	// Write Greetings Message
	MSS_UART_polled_tx_string(&g_mss_uart0, g_greeting_msg);

	// Initialize I2C interface for communication with ALS
	MSS_I2C_init(&g_mss_i2c0,
				  0x0,	// address if in slave configuration - not needed here
				  MSS_I2C_PCLK_DIV_960);

	// Write I2C Initialization Message
	MSS_UART_polled_tx_string(&g_mss_uart0, g_i2c_init_msg);

	// Initialize ALS
	uint8_t ok = ALS_init();
	if(ok!=0){
		uint8_t tmp[2] = " ";
		tmp[0] = ok;
		MSS_UART_polled_tx_string(&g_mss_uart0, tmp);
		return 0;
	}
	// Write ALS Initialization Message
	MSS_UART_polled_tx_string(&g_mss_uart0, g_als_init_msg);

	// GPIO Init
	MSS_GPIO_init();

	MSS_GPIO_config( MSS_GPIO_0, MSS_GPIO_OUTPUT_MODE); 	// LED0
	MSS_GPIO_config( GPIO_LED, MSS_GPIO_OUTPUT_MODE); 	// pin3 on 2x4 header

	MSS_GPIO_config( MSS_GPIO_8, MSS_GPIO_INPUT_MODE); 		// SW1
	//MSS_GPIO_config( MSS_GPIO_9, MSS_GPIO_INPUT_MODE | MSS_GPIO_IRQ_EDGE_POSITIVE );
	//MSS_GPIO_enable_irq(MSS_GPIO_8);

	// Timer1 Init, controls the time slot
	MSS_TIM1_init(MSS_TIMER_PERIODIC_MODE);
	//MSS_TIM1_load_background(SystemCoreClock);
	//MSS_TIM1_enable_irq();
	//MSS_TIM1_start();

	// Timer2 Init, generates PWM signal (duty_cycle 50%) on GPIO_8, HW pin 81
	MSS_TIM2_init(MSS_TIMER_PERIODIC_MODE);
	//MSS_TIM2_load_background(SystemCoreClock / 18);
	//MSS_TIM2_enable_irq();
	//MSS_TIM2_start();

	// SPI1 Init
	MSS_SPI_init( &g_mss_spi1 );
	MSS_SPI_configure_master_mode (&g_mss_spi1, MSS_SPI_SLAVE_0, MSS_SPI_MODE2, 512u, 16);

	uint32_t current_button = 0;	// current value of the push button
	uint32_t previous_button = 0;	// last state of the push button

	uint8_t start_flag = 0;			// this flag indicates start of the pwm frequency sweep


	uint8_t new_als_data = 0;
	/* als_raw_data
	 *  buffer containing the raw data from als
	 *  als_raw_data[0] - CH1 LO
	 *  als_raw_data[1] - CH1 HI
	 *  als_raw_data[2] - CH0 LO
	 *  als_raw_data[3] - CH0 HI
	 */
	uint8_t als_raw_data[4] = {0, 0, 0, 0};
	uint8_t *als_formated_data;	// string of formated als data
	// Main loop
	while(1){
		if(start_flag){ 												// start the frequency sweep
			static uint32_t point_cnt = 0;								// point counter tells us at which point in decade are we
			static uint32_t decade_cnt = NUM_OF_DECADES;				// whit decade counter we adjust the frequency based on current decade

			if(change_freq){											// New time slot, we can change the frequency
				MSS_TIM2_disable_irq();									// disable Timer2 interrupt so we don't get unexpected interrupts
				MSS_TIM2_stop();										// no need to run Timer2 at this point

				double freq;
				if(point_cnt == 0) freq = (point_cnt + 1)*pow(10, NUM_OF_DECADES - decade_cnt); // calculate the frequency for next time slot based on current point and decade
				else freq = (FREQ_STEP*point_cnt + 1)*pow(10, NUM_OF_DECADES - decade_cnt);

				MSS_TIM2_load_background(SystemCoreClock / (freq*2));	// Set the timer value based on desired frequency, *2 because we are toggeling the pin in Timer2 ISR

				MSS_SPI_set_slave_select(&g_mss_spi1, MSS_SPI_SLAVE_0);
				uint16_t control_reg = (CONTROL_REG | WRITE_FREQ_28 | OUTPUT_SINE);
				MSS_SPI_transfer_frame(&g_mss_spi1, control_reg);

				uint32_t freq_reg = ((double)freq / REF_FREQ) * 0xFFFFFFF;
				uint16_t freq_reg_lsb = freq_reg & 0x00003FFF;
				uint16_t freq_reg_msb = (freq_reg & 0xFFFC000 ) >> 14;
				MSS_SPI_transfer_frame(&g_mss_spi1, freq_reg_lsb | FREQ_REG_0);
				MSS_SPI_transfer_frame(&g_mss_spi1, freq_reg_msb | FREQ_REG_0);

				MSS_SPI_clear_slave_select(&g_mss_spi1, MSS_SPI_SLAVE_0);

				point_cnt++;											// next point
				if(point_cnt >= POINTS_PER_DECADE-1){					// check if all points in decade were executed
					point_cnt = 0;										// if so, reset the point counter
					decade_cnt--;										// and switch to a new decade
					if(decade_cnt <= 0){								// check if all decades were executed
						freq = (point_cnt + 1)*pow(10, NUM_OF_DECADES - decade_cnt);
						MSS_TIM2_load_background(SystemCoreClock / (freq*2));

						MSS_SPI_set_slave_select(&g_mss_spi1, MSS_SPI_SLAVE_0);
						control_reg = (CONTROL_REG | WRITE_FREQ_28 | OUTPUT_SINE);
						MSS_SPI_transfer_frame(&g_mss_spi1, control_reg);

						freq_reg = ((double)freq / REF_FREQ) * 0xFFFFFFF;
						freq_reg_lsb = freq_reg & 0x00003FFF;
						freq_reg_msb = (freq_reg & 0xFFFC000 ) >> 14;
						MSS_SPI_transfer_frame(&g_mss_spi1, freq_reg_lsb | FREQ_REG_0);
						MSS_SPI_transfer_frame(&g_mss_spi1, freq_reg_msb | FREQ_REG_0);

						MSS_SPI_clear_slave_select(&g_mss_spi1, MSS_SPI_SLAVE_0);

						decade_cnt = NUM_OF_DECADES; 					// if so, prepare variable for another sweep
						start_flag = 0; 								// indicate the end of frequency sweep by setting the start flag to 0
						MSS_GPIO_set_output(MSS_GPIO_0, 1); 			// also turn off the LED to indicate the end of the sweep

						MSS_TIM1_disable_irq();							// disable Timer1 interrupt, no need for it at this point
						MSS_TIM1_stop();								// no need to run Timer1 at this point
					}
				}

				// Notify via UART that a new frequency is set
				MSS_UART_polled_tx_string(&g_mss_uart0, g_new_freq_mess);

				change_freq = 0;										// reset the flag for the next available time slot
				MSS_TIM2_enable_irq();									// enable Timer2 interrupt
				MSS_TIM2_start();										// start Timer2 or PWM
			}

			// Read the sensor new values
			new_als_data = read_als(als_raw_data);
			if(new_als_data==1){
				// Format the raw data into string for sending via UART
				als_formated_data = format_als_raw_data(als_raw_data);
				// Send new data via UART
				MSS_UART_polled_tx_string(&g_mss_uart0, als_formated_data);
				free(als_formated_data);
				new_als_data = 0;
			}
		}else{															// if start flag is not set wait for the next button press
																		// detect positive edge when sw1 is pressed
			current_button = MSS_GPIO_get_inputs() & (1 << MSS_GPIO_8);
			if(current_button > 0 && previous_button == 0){
				//static uint8_t led_status = 0xFF;
				//led_status = ~led_status;
				MSS_GPIO_set_output(MSS_GPIO_0, 0);

				start_flag = 1;											// when positive edge is detected set the start flag
				// start Timer1
				MSS_TIM1_load_background(SystemCoreClock);				// and enable Timer1
				MSS_TIM1_enable_irq();
				MSS_TIM1_start();

				previous_button = 1;
			}else{
				previous_button = current_button;
			}

		}
	}

	return 0;
}

// Timer1 IRQ handler is used to determine time slot length
void Timer1_IRQHandler(void){
	static uint32_t cnt = 0;

	cnt++;
	if(cnt > TIME_SLOT){
		cnt = 0;
		change_freq = 1;
	}
	MSS_TIM1_clear_irq();
}

// Timer2 IRQ handler or in other words PWM generator
void Timer2_IRQHandler(void){
	static uint8_t gpio_output = 0xFF;

	gpio_output = ~gpio_output;
	MSS_GPIO_set_output(GPIO_LED, gpio_output);

	MSS_TIM2_clear_irq();
}

