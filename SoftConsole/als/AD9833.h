/*
 * AD9833.h
 *
 *  Created on: 17. jan. 2019
 *      Author: Gasper
 */

#ifndef AD9833_H_
#define AD9833_H_

#define FREQ_REG_0				(1 << 14)
#define FREQ_REG_1				(1 << 15)
#define WRITE_FREQ_28			(1 << 13) // Write 28bits to freq register, two consecutive writes

#define PHASE_REG_0				0xC000
#define PHASE_REG_1				0xE000

#define CONTROL_REG				0x0000
#define SELECT_FREQ_REG_1 		(1 << 11)
#define SELECT_PHASE_REG_1 		(1 << 10)

#define RESET					(1 << 8)

#define SLEEP1 					(1 << 7)
#define SLEEP12					(1 << 6)
#define DAC_POWER_DOWN 			SLEEP12
#define DISABLE_INTERNAL_CLK	SLEEP1
#define POWER_DOWN				SLEEP1 | SLEEP12

#define OPBITEN 				(1 << 5)
#define DIV2					(1 << 3)
#define MODE					(1 << 1)
#define OUTPUT_SINE				0x0000
#define OUTPUT_TRIANGLE			MODE
#define OUTPUT_DAC_MSB_2		OPBITEN
#define OUTPUT_DAC_MSB			OPBITEN | MODE

#define REF_FREQ				25000000u

#endif /* AD9833_H_ */
