# Program reads data of frequency response measurements
# and plots the frequency's response amplitude.
#
# Author: Uros Hudomalj & Gasper Skvarc Bozic
# Last revision: 23.01.2019

#################      IMPORTS      #################
# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import math

#################     CONSTANTS     #################
ORIGINAL_DATASETS = {'pwm':'pwm_50.txt', 'dac':'sin_50.txt', 'ref':'ref.txt', 'pwm10':'pwm_10.txt', 'dac10':'sin_10.txt',}    # path to the input dataset
POINTS_PER_DEC = {'pwm':50, 'dac':50, 'ref':10, 'pwm10':10, 'dac10':10}               # measured points per decade

PLT_TITLES = {'pwm':'LED driven with \nsquare signal - 50 points per decade',
              'dac':'LED driven with \nsinus signal from DAC - 50 points per decade',
              'ref':'LED driven with \nsinus signal from function generator - 10 points per decade', 
              'pwm10':'LED driven with \nsquare signal - 10 points per decade', 
              'dac10':'LED driven with \nsinus signal from DAC - 10 points per decade'}    # titles of the plots for each dataset

#################      PROGRAM      #################

print("Starting program.")

channels = {'ch0': {}, 'ch1': {}}       # for containing measurements of individual channel for all frequencies
freq = {}                               # list of frequencies measured (starting from 1 Hz and up with POINTS_PER_DEC frequencies per decade)

freq_res = {'ch0': {}, 'ch1': {}}       # for containing claculated frequency response

for dataset_name in ORIGINAL_DATASETS:

    f = open(ORIGINAL_DATASETS[dataset_name], "r")
    f = list(f)
    
    channels['ch0'][dataset_name] = []
    channels['ch1'][dataset_name] = []
    freq[dataset_name] = []

    added_row = False
    c = 0   # counter for generating frequencies
    rc = 0  # row counter
    decade = 0

    for i, xi in enumerate(f):
        if xi[8]==',':
            # append next measurement value to each channel
            tmp = float(xi[0:8])
            channels['ch0'][dataset_name][rc-1].append(tmp)
            tmp = float(xi[10:18])
            channels['ch1'][dataset_name][rc-1].append(tmp)
            added_row = False
        elif added_row==False and i!=len(f)-1:                # if it is not the last row
            
            rc += 1
            # add new row to both channels = new frequency
            for ch in channels:
                channels[ch][dataset_name].append([])
            # add the frequency
            if(c == 0):
                tmp = (c+1)
            else:
                tmp = float((10-1)/(POINTS_PER_DEC[dataset_name]-1))*(c%POINTS_PER_DEC[dataset_name]) +1
            tmp *= 10**decade
    	
            freq[dataset_name].append(tmp)
            c += 1
            if(c >= POINTS_PER_DEC[dataset_name]-1):
                decade += 1
                c = 0
            added_row = True

    print("Extracted measurements for dataset: {}.".format(dataset_name))

    # Calculate frequency response = get amplitudes of the inner part
    for ch in channels:
        # For each channel's frequency
        freq_res[ch][dataset_name] = []
        for ifr, fr in enumerate(channels[ch][dataset_name]):
            # Get the inner part (10% - 90%)
            lo = math.floor(0.1*len(channels[ch][dataset_name][ifr]))
            hi = math.floor(0.9*len(channels[ch][dataset_name][ifr]))
            channels[ch][dataset_name][ifr] = channels[ch][dataset_name][ifr][lo:hi]

            # Get the amplitude
            pp = max(channels[ch][dataset_name][ifr]) - min(channels[ch][dataset_name][ifr])
            amp = pp/2

            amp_db = 20*math.log10(amp)     # convert to decibels

            # Add amplitude to frequnecy response
            freq_res[ch][dataset_name].append(amp_db)

    # Normalize to max 0 dB
    for ch in channels:
        max_val = max(freq_res[ch][dataset_name])
        freq_res[ch][dataset_name] = [x-max_val for x in freq_res[ch][dataset_name]]

    if dataset_name == 'dac10':
        for ch in channels:
            freq_res[ch][dataset_name][-1] = freq_res[ch][dataset_name][-1]-16.5 # there was an error in the measurements at the last sample

    print("Frequency response calculated.")

    # Convert to np.arrays for plotting?
    for ch in freq_res:
        freq_res[ch][dataset_name] = np.array(freq_res[ch][dataset_name])
    freq[dataset_name] = np.array(freq[dataset_name])

    # Plot the frequency response for both channels
    print("Plotting response of: {}.".format(dataset_name))
    for ch in channels:
        plt.plot(freq[dataset_name], freq_res[ch][dataset_name])
        plt.xlabel('Freq [Hz]')
        plt.ylabel('Gain [dB]')
        plt.xlim((1,1000))
        plt.ylim((-60,1))
        plt.title(PLT_TITLES[dataset_name])
        plt.xscale('log')
        plt.grid(True, which="both")

    plt.show(block=True)

# Compare responses for just 1 channel
print("Plotting all responses together.")
for dataset_name in ORIGINAL_DATASETS:
    if dataset_name != 'pwm10' and dataset_name != 'dac10':
        plt.plot(freq[dataset_name], freq_res['ch0'][dataset_name], label = dataset_name)
plt.xlabel('Freq [Hz]')
plt.ylabel('Gain [dB]')
plt.xlim((1,1000))
plt.ylim((-60,1))
plt.title("All different measured responses for one channel")
plt.xscale('log')
plt.grid(True, which="both")
plt.legend()

plt.show(block=True)