This repo contains project for measuring frequency response of Ambient Light Sensor
(LTR-329ALS-01).
Project is based on Microsemi Smartfuion2 Maker Kit, on which the ALS is integrated.
For producing sinus wave needed for the measuring of the response an AD9833 with
added push-pull configuration was used to drive an LED.
For configuring Smartfuion2 Maker Kit Microsemi Libero and SoftConsole were used.

Most important repo contents:
- ALS.prjx: Microsemi Libero project for configuring Smartfuion2 Maker Kit
- SoftConsole/als: files for programming Smartfuion2 Maker Kit
	- /drivers: libraries produced by Libero
	- AD9833.h: defines for setting AD9833
	- als.h/.c: defines and code for accessing LTR-329ALS-01 ALS
	- main.c: program for measuring ALS frequency response
- Project_Ambient_Light_Sensor_Filter_Final_Presentation.pptx: presentation of the project

Project was done as a project work for course "Integrated Systems for Industry and 
Space Applications" at Technical University of Munich (TUM).

All contents of the repo are original work of Uros Hudomalj (uros.hudomalj@tum.de)
and Gasper Skvarc Bozic (gasper.skvarc@tum.de).